import './bootstrap';
import 'firebase/compat/firestore';
import { initializeApp } from "@firebase/app";
import { getFirestore, collection, doc, addDoc, setDoc, onSnapshot, getDoc, updateDoc } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCvOA4tncYV09sRAq8TaxaezK6mjO1oED8",
  authDomain: "xabiru-27bf5.firebaseapp.com",
  projectId: "xabiru-27bf5",
  storageBucket: "xabiru-27bf5.appspot.com",
  messagingSenderId: "994074485425",
  appId: "1:994074485425:web:c943d334753dfc9274b890"
};

const app = initializeApp(firebaseConfig);
const firestore = getFirestore();

// server config
const servers = {
    iceServers: [
      {
        urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'],
      },
    ],
    iceCandidatePoolSize: 10,
  };

  // Global State
  const pc = new RTCPeerConnection(servers);
  let localStream = null;
  let remoteStream = null;

  // HTML elements
  const webcamButton = document.getElementById('webcamButton');
  const webcamVideo = document.getElementById('webcamVideo');
  const callButton = document.getElementById('callButton');
  const callInput = document.getElementById('callInput');
  const answerButton = document.getElementById('answerButton');
  const remoteVideo = document.getElementById('remoteVideo');
  const hangupButton = document.getElementById('hangupButton');

  // 1. Setup media sources

  webcamButton.onclick = async () => {
    localStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
    remoteStream = new MediaStream();

    // Push tracks from local stream to peer connection
    localStream.getTracks().forEach((track) => {
      pc.addTrack(track, localStream);
    });

    // Pull tracks from remote stream, add to video stream
    pc.ontrack = (event) => {
      event.streams[0].getTracks().forEach((track) => {
        remoteStream.addTrack(track);
      });
    };

    webcamVideo.srcObject = localStream;
    remoteVideo.srcObject = remoteStream;

    callButton.disabled = false;
    answerButton.disabled = false;
    webcamButton.disabled = true;
  };

  // 2. Create an offer
  callButton.onclick = async () => {
    // Reference Firestore collections for signaling
    const docReff = collection(firestore,'calls');
    const callDoc = doc(docReff);
    const offerCandidates = collection(callDoc,'offerCandidates');
    const answerCandidates = collection(callDoc,'answerCandidates');

    callInput.value = callDoc.id;

    // Get candidates for caller, save to db
    pc.onicecandidate = (event) => {
      event.candidate && addDoc(offerCandidates,event.candidate.toJSON());
    };

    // Create offer
    const offerDescription = await pc.createOffer();
    await pc.setLocalDescription(offerDescription);

    const offer = {
      sdp: offerDescription.sdp,
      type: offerDescription.type,
    };

    await setDoc(callDoc,{ offer });

    // Listen for remote answer
    onSnapshot(callDoc, (snapshot) => {
      const data = snapshot.data();
      if (!pc.currentRemoteDescription && data?.answer) {
        const answerDescription = new RTCSessionDescription(data.answer);
        pc.setRemoteDescription(answerDescription);
      }
    });

    // When answered, add candidate to peer connection
    onSnapshot(answerCandidates, (snapshot) => {
      snapshot.docChanges().forEach((change) => {
        if (change.type === 'added') {
          const candidate = new RTCIceCandidate(change.doc.data());
          pc.addIceCandidate(candidate);
        }
      });
    });

    hangupButton.disabled = false;
  };

  // 3. Answer the call with the unique ID
  answerButton.onclick = async () => {
    const callId = callInput.value;
    const docReff = collection(firestore,'calls');
    const callDoc = doc(docReff,callId);
    const answerCandidates = collection(callDoc,'answerCandidates');
    const offerCandidates = collection(callDoc,'offerCandidates');

    pc.onicecandidate = (event) => {
      event.candidate && addDoc(answerCandidates,event.candidate.toJSON());
    };

    const callData = (await getDoc(callDoc)).data();

    const offerDescription = callData.offer;
    await pc.setRemoteDescription(new RTCSessionDescription(offerDescription));

    const answerDescription = await pc.createAnswer();
    await pc.setLocalDescription(answerDescription);

    const answer = {
      type: answerDescription.type,
      sdp: answerDescription.sdp,
    };

    await updateDoc(callDoc,{ answer });

    onSnapshot(offerCandidates,(snapshot) => {
      snapshot.docChanges().forEach((change) => {
        console.log(change);
        if (change.type === 'added') {
          let data = change.doc.data();
          pc.addIceCandidate(new RTCIceCandidate(data));
        }
      });
    });
  };
