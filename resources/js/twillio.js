const Video = require('twilio-video');
Video.connect("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzBlODg0MWMyMTFlNGE3Njg3N2E5YTViNjYwZDVlOTVlLTE2NjU0ODIzOTUiLCJpc3MiOiJTSzBlODg0MWMyMTFlNGE3Njg3N2E5YTViNjYwZDVlOTVlIiwic3ViIjoiQUM2ZjZjMmYwZjczODNhMjkyMWE2NDViNzc0OTUyMDlmMiIsImV4cCI6MTY2NTQ4NTk5NSwiZ3JhbnRzIjp7ImlkZW50aXR5Ijoiam9obl9kb2UiLCJjaGF0Ijp7InNlcnZpY2Vfc2lkIjoiSVMwNmE0NmE4OGY2MTQ0NjNlOTlmYmQ1Y2NjOWY3MzdlMiJ9LCJ2b2ljZSI6e30sInZpZGVvIjp7fX19.rercEN-SZDBchWMrFHfmshUx_mWP6MQ-6cmvUcObslg", { name: 'WidihCok' }).then(room => {
    console.log('Connected to Room "%s"', room.name);

    room.participants.forEach(participantConnected);
    room.on('participantConnected', participantConnected);

    room.on('participantDisconnected', participantDisconnected);
    room.once('disconnected', error => room.participants.forEach(participantDisconnected));
  });
