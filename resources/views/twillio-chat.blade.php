<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="favicon.svg" />
    <link rel="stylesheet" href="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WebRTC</title>
    {{-- App Css --}}
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
  </head>
  <body>


    <!-- script for webrtc -->
    <script src="{{ mix('js/twillio.js') }}"></script>

  </body>
</html>
