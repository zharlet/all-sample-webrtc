<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Jwt\Grants\VoiceGrant;

class TwillioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $twilioAccountSid = 'AC6f6c2f0f7383a2921a645b77495209f2';
        // $twilioApiKey = 'SK0e8841c211e4a76877a9a5b660d5e95e';
        // $twilioApiSecret = 'WYwhhmHyH8EnfOWqw6SQhqja48ZsfB8H';

        // // Required for Chat grant
        // $serviceSid = 'IS06a46a88f614463e99fbd5ccc9f737e2';
        // // choose a random username for the connecting user
        // $identity = "john_doe";
        // $token = new AccessToken(
        //     $twilioAccountSid,
        //     $twilioApiKey,
        //     $twilioApiSecret,
        //     3600,
        //     $identity
        // );
        // // Create Chat grant
        // $chatGrant = new ChatGrant();
        // $chatGrant->setServiceSid($serviceSid);
        // $voiceGrant = new VoiceGrant();
        // $videoGrant = new VideoGrant();
        // // Add grant to token
        // $token->addGrant($chatGrant);
        // $token->addGrant($voiceGrant);
        // $token->addGrant($videoGrant);

        // // render token to string
        // dd($token->toJWT());
        // return view('twillio-chat');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
